/**
 * Given the bindings and the payload, it sets the variables value
 * @param {Array[Object]} bindings Bindings
 * @param {Object} data Payload
 */
const setCssVariables = (bindings, data) => {
  const rootDOM = document.documentElement
  bindings.forEach(el => {
    if (data[el.from] !== undefined) {
      rootDOM.style.setProperty(el.to, `${el.prefix ? el.prefix : ''}${data[el.from]}${el.suffix ? el.suffix : ''}`)
    }
  })
}

/**
 * Generates the plugin given the mutation to watch and the bindings
 * @param {Object} config Config object
 * @param {String} config.mutation Mutation to watch (the variables will be updated when this mutation is called)
 * @param {Array[Object]} config.bindings Array of relations between the payload and the CSS variables
 * @param {String} config.bindings.from Payload property to get the value from
 * @param {String} config.bindings.to CSS Variable to modify
 * @param {String} config.bindings.prefix Prefix to place before the value
 * @param {String} config.bindings.suffix Suffix to place after the value
 * @example
 * const syncCss = require('vuex-css-sync')
 *
 * const bindings = [
 *   {
 *     from: 'color_bg',
 *     to: '--custom-color-bg',
 *     prefix: '#'
 *   },
 *   {
 *     from: 'color_font',
 *     to: '--custom-color-font',
 *     prefix: '#'
 *   }
 * ]
 *
 * const plugin = syncCss({
 *   bindings,
 *   mutation: 'SET_VARIABLES'
 * })
 *
 * const store = new Vuex.Store({
 *   state,
 *   mutations,
 *   plugins: [plugin]
 * })
 */
const generatePlugin = (config) => {
  return (store) => {
    store.subscribe((mutation, state) => {
      if (mutation.type === config.mutation) {
        setCssVariables(config.bindings, mutation.payload)
      }
    })
  }
}

export default generatePlugin
